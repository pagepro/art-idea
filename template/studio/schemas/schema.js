import createSchema from 'part:@sanity/base/schema-creator'
import schemaTypes from 'all:part:@sanity/base/schema-type'

// document schemas
import category from './documents/category'
import page from './documents/page'
import headerPicture from './documents/headerPicture'
import preset from './documents/preset'

// Object types
import content from './objects/content'

export default createSchema({
  name: 'default',
  types: schemaTypes.concat([category, page, preset, content, headerPicture])
})
