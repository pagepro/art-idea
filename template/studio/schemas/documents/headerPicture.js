export default {
  name: 'headerPicture',
  type: 'document',
  title: 'Header Picture',
  fieldsets: [
    {
      name: 'bottomLink',
      title: 'Bottom link'
    }
  ],
  fields: [
    {
      type: 'string',
      name: 'title'
    },
    {
      type: 'image',
      name: 'image',
      options: {
        hotspot: true
      }
    },
    {
      fieldset: 'bottomLink',
      type: 'string',
      name: 'text'
    },
    {
      fieldset: 'bottomLink',
      type: 'url',
      name: 'url',
      validation: Rule =>
        Rule.uri({
          scheme: ['http', 'https']
        })
    }
  ],
  preview: {
    select: {
      title: 'title',
      subtitle: 'text',
      media: 'image'
    },
    prepare({ title = 'Header Picture', subtitle, media }) {
      return {
        title,
        subtitle,
        media
      }
    }
  }
}
