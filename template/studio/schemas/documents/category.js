export default {
  name: 'category',
  type: 'document',
  title: 'Category',
  fields: [
    {
      name: 'name',
      type: 'string',
    },
    {
      name: 'slug',
      type: 'slug',
      options: {
        source: 'name',
        maxLength: 96,
      },
    },
    {
      name: 'parametersList',
      type: 'array',
      title: 'List of Parameter Options',
      of: [{ type: 'string' }],
    },
  ],
  preview: {
    select: {
      title: 'name',
      parameters: 'parametersList',
    },
    prepare({ title = 'New Category', parameters }) {
      const listOfParameters = Object.values(parameters || {})
        .filter(Boolean)
        .join(', ')

      return {
        title,
        subtitle: listOfParameters,
      }
    },
  },
}
