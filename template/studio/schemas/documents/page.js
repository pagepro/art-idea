export default {
  name: 'page',
  type: 'document',
  title: 'Page',
  fieldsets: [
    {
      name: 'seo',
      title: 'SEO'
    },
    {
      name: 'header',
      title: 'Header'
    },
    {
      name: 'button',
      title: 'Button',
      description: 'Button next to page title'
    },
    {
      name: 'presetButton',
      title: 'Presets link button',
      description: 'Button next to presets dropdown'
    }
  ],
  fields: [
    {
      name: 'title',
      type: 'string'
    },
    {
      name: 'slug',
      type: 'slug',
      validation: Rule => Rule.required(),
      options: {
        source: 'title',
        maxLength: 96
      }
    },
    {
      name: 'h1Text',
      fieldset: 'header',
      type: 'string',
      title: 'H1 text'
    },
    {
      name: 'subtitleUrl',
      type: 'url',
      fieldset: 'header',
      validation: Rule =>
        Rule.uri({
          scheme: ['http', 'https']
        })
    },
    {
      name: 'subtitleText',
      type: 'string',
      fieldset: 'header'
    },
    {
      name: 'descriptionText',
      fieldset: 'header',
      type: 'string'
    },
    {
      name: 'adCode',
      fieldset: 'header',
      type: 'text'
    },
    {
      name: 'showBriefGenerator',
      type: 'boolean',
      options: {
        layout: 'checkbox'
      }
    },
    {
      name: 'content',
      type: 'content'
    },
    {
      name: 'presetsButtonUrl',
      title: 'Url',
      type: 'url',
      fieldset: 'presetButton',
      validation: Rule =>
        Rule.uri({
          scheme: ['http', 'https']
        })
    },
    {
      name: 'presetsButtonText',
      title: 'Text',
      type: 'string',
      fieldset: 'presetButton'
    },
    {
      name: 'buttonLink',
      title: 'Url',
      type: 'url',
      fieldset: 'button',
      validation: Rule =>
        Rule.uri({
          scheme: ['http', 'https']
        })
    },
    {
      name: 'buttonText',
      title: 'Text',
      type: 'string',
      fieldset: 'button'
    },
    {
      name: 'seoTitle',
      fieldset: 'seo',
      title: 'Title',
      type: 'string'
    },
    {
      name: 'seoDescription',
      fieldset: 'seo',
      title: 'Description',
      type: 'text'
    }
  ],
  initialValue: {
    showBriefGenerator: false
  },
  preview: {
    select: {
      title: 'title',
      subtitle: 'slug.current'
    },
    prepare({ title = 'New page', subtitle }) {
      return {
        title,
        subtitle
      }
    }
  }
}
