export default {
  name: 'preset',
  type: 'document',
  title: 'Preset',
  fields: [
    {
      type: 'string',
      name: 'name'
    },
    {
      type: 'array',
      name: 'categories',
      validation: Rule => Rule.unique(),
      of: [
        {
          type: 'reference',
          to: { type: 'category' }
        }
      ]
    },

    {
      type: 'reference',
      name: 'headerPicture',
      to: [{ type: 'headerPicture' }]
    }
  ],
  preview: {
    select: {
      title: 'name',
      subtitle: 'text',
      media: 'image'
    },
    prepare({ title = 'Preset', subtitle, media }) {
      return {
        title,
        subtitle,
        media
      }
    }
  }
}
