import S from '@sanity/desk-tool/structure-builder'
import { MdLocalOffer, MdWeb, MdPhotoLibrary, MdLibraryBooks } from 'react-icons/md'
import IframePreview from '../previews/IframePreview'

// Web preview configuration
const remoteURL = 'https://<#<deployments.web.url>#>'
const localURL = 'http://localhost:8000'
const previewURL = window.location.hostname === 'localhost' ? localURL : remoteURL

export const getDefaultDocumentNode = ({ schemaType }) => {
  if (['page'].includes(schemaType)) {
    return S.document().views([
      S.view.form(),
      S.view
        .component(IframePreview)
        .title('Web preview')
        .options({ previewURL })
    ])
  }
  return S.document().views([S.view.form()])
}

export default () =>
  S.list()
    .title('Content')
    .items([
      S.listItem()
        .title('Categories')
        .icon(MdLocalOffer)
        .schemaType('category')
        .child(S.documentTypeList('category').title('Categories')),
      S.listItem()
        .title('Pages')
        .icon(MdWeb)
        .schemaType('page')
        .child(S.documentTypeList('page').title('Pages')),
      S.listItem()
        .title('Header pictures')
        .icon(MdPhotoLibrary)
        .schemaType('headerPicture')
        .child(S.documentTypeList('headerPicture').title('Header pictures')),
      S.listItem()
        .title('Presets')
        .icon(MdLibraryBooks)
        .schemaType('preset')
        .child(S.documentTypeList('preset').title('Presets')),
      ...S.documentTypeListItems().filter(
        listItem => !['category', 'page', 'headerPicture', 'preset'].includes(listItem.getId())
      )
    ])
