export default {
  widgets: [
    { name: 'structure-menu' },
    {
      name: 'project-info',
      options: {
        __experimental_before: [
          {
            name: 'netlify',
            options: {
              description:
                'NOTE: Because these sites are static builds, they need to be re-deployed to see the changes when documents are published.',
              sites: [
                {
                  buildHookId: '<#<deployments.web.providerInfo.buildHookId>#>',
                  title: 'Art Idea Website',
                  name: '<#<deployments.web.providerInfo.siteName>#>',
                  apiId: '<#<deployments.web.providerInfo.siteId>#>',
                },
                {
                  buildHookId: '<#<deployments.dev.providerInfo.buildHookId>#>',
                  title: 'Art Idea Website - DEV',
                  name: '<#<deployments.dev.providerInfo.siteName>#>',
                  apiId: '<#<deployments.dev.providerInfo.siteId>#>',
                },
              ],
            },
          },
        ],
        data: [
          {
            title: 'Gitlab repo',
            value: 'https://gitlab.com/<#<repository.owner>#>/<#<repository.name>#>',
            category: 'Code',
          },
          { title: 'Frontend', value: 'https://<#<deployments.web.url>#>', category: 'apps' },
        ],
      },
    },
    { name: 'project-users', layout: { height: 'auto' } },
  ],
}
