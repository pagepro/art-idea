const isProd = false;

require('dotenv').config({
  path: `.env.${process.env.NODE_ENV || 'development'}`,
});

module.exports = {
  siteMetadata: {
    title: `Brief Builder`,
    description: `Use this app to help generate ideas for your artistic exercises or projects.`,
    author: `Pagepro`,
  },
  plugins: [
    {
      resolve: 'gatsby-plugin-root-import',
      options: {
        src: `${__dirname}/src`,
        components: `${__dirname}/src/components`,
        pages: `${__dirname}/src/pages`,
        assets: `${__dirname}/src/assets`,
        hooks: `${__dirname}/src/hooks`,
        setup: `${__dirname}/src/setup`,
        routes: `${__dirname}/src/routes`,
      },
    },
    `gatsby-plugin-theme-ui`,
    `gatsby-plugin-emotion`,
    `gatsby-plugin-react-svg`,
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-typescript`,
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: 'images',
        path: `${__dirname}/src/assets/images`,
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        icon: `src/assets/images/favicon.png`,
      },
    },
    {
      resolve: 'gatsby-plugin-google-tagmanager',
      options: {
        id: process.env.GTM_KEY,
        includeInDevelopment: false,
      },
    },
    {
      resolve: 'gatsby-source-sanity',
      options: {
        projectId:
          process.env.GATSBY_SANITY_PROJECT_ID || '<#< sanity.projectId >#>',
        dataset: process.env.GATSBY_SANITY_DATASET || '<#< sanity.dataset >#>',
        token: process.env.SANITY_READ_TOKEN,
        watchMode: !isProd,
        overlayDrafts: !isProd,
      },
    },
  ],
};
