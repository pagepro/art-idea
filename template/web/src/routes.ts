const routes = {
  HOME: '/',
  ABOUT: '/about',
  CHANGELOG: '/changelog',
};

export default routes;
