export interface ParametersListProps {
  items: string[];
  itemsToShowCount?: number;
  otherPossibilitiesCount?: number | null;
  onItemSelect?: (itemIndex: number, eventType: 'click' | 'mouseLeave') => void;
  category?: string;
}
