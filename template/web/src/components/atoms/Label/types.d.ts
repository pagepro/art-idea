import { TextProps } from 'theme-ui';

export interface LabelProps extends TextProps {
  label: string | JSX.Element;
}
