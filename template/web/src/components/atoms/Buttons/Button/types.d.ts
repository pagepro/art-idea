import { ButtonHTMLAttributes } from 'react';
import { ButtonProps as ThemedButtonProps } from 'theme-ui';

export enum ButtonVariant {
  primary = 'primary',
}

export interface ButtonProps
  extends ButtonHTMLAttributes<HTMLButtonElement>,
    ThemedButtonProps {
  variant?: ButtonVariant;
}
