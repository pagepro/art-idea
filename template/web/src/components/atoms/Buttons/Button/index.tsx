/** @jsx jsx */
import { jsx, Button as ThemedButton } from 'theme-ui';

import { ButtonProps } from './types';

const Button: React.FC<ButtonProps> = (props) => <ThemedButton {...props} />;

export default Button;
