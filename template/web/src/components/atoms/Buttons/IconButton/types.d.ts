import { ButtonHTMLAttributes } from 'react';
import { ButtonProps } from 'theme-ui';

export interface IconButtonProps
  extends ButtonHTMLAttributes<HTMLButtonElement>,
    ButtonProps {}
