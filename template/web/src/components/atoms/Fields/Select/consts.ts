import { SelectItem } from './types';

export const demoSelectThemeItems: SelectItem[] = [
  { value: 'Theme', label: 'Theme' },
  { value: 'Character', label: 'Character' },
  { value: 'Location', label: 'Location', isDisabled: true },
  { value: 'Scale', label: 'Scale' },
  { value: 'Emotion', label: 'Emotion' },
  { value: 'Time Period', label: 'Time Period' },
  { value: 'Functionality', label: 'Functionality' },
];

export const demoSelectLocationItems: SelectItem[] = [
  { value: 'Location', label: 'Location' },
  { value: 'Character', label: 'Character' },
  { value: 'Scale', label: 'Scale' },
  { value: 'Emotion', label: 'Emotion' },
  { value: 'Time Period', label: 'Time Period', isDisabled: true },
  { value: 'Functionality', label: 'Functionality' },
];

export const demoSelectArchitectureItems: SelectItem[] = [
  { value: 'Character', label: 'Character' },
  { value: 'Location', label: 'Location', isDisabled: true },
  { value: 'Scale', label: 'Scale' },
  { value: 'Emotion', label: 'Emotion' },
  { value: 'Time Period', label: 'Time Period' },
  { value: 'Functionality', label: 'Functionality' },
];

export const demoSelectAtmosphereItems: SelectItem[] = [
  { value: 'Atmosphere', label: 'Atmosphere' },
  { value: 'Character', label: 'Character' },
  { value: 'Location', label: 'Location', isDisabled: true },
  { value: 'Scale', label: 'Scale' },
  { value: 'Emotion', label: 'Emotion' },
  { value: 'Time Period', label: 'Time Period' },
  { value: 'Functionality', label: 'Functionality' },
];

export const demoSelectTimePeriodItems: SelectItem[] = [
  { value: 'Time Period', label: 'Time Period' },
  { value: 'Character', label: 'Character' },
  { value: 'Location', label: 'Location', isDisabled: true },
  { value: 'Scale', label: 'Scale' },
  { value: 'Emotion', label: 'Emotion' },
  { value: 'Functionality', label: 'Functionality' },
];

export const demoSelectPresetsItems: SelectItem[] = [
  { value: 'Intro to 3D Concept Design', label: 'Intro to 3D Concept Design' },
  {
    value: 'Designing Better Characters',
    label: 'Designing Better Characters',
  },
];
