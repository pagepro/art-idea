import { Props as ReactSelectProps } from 'react-select';
import { FieldWrapperCommonProps } from '../FieldWrapper/types';
import { FieldSize } from '../types';

export interface SelectItem {
  value: string;
  label: string;
  isDisabled?: boolean;
}

export interface SelectProps
  extends ReactSelectProps<SelectItem>,
    FieldWrapperCommonProps {
  size?: FieldSize;
}
