export interface FieldWrapperCommonProps {
  label?: string;
}

export interface FieldWrapperProps extends FieldWrapperCommonProps {
  controller: JSX.Element;
}
