export default {
  title: {
    marginRight: ['', '', '1.5rem'],
    display: ['none', '', 'block'],
  },
};
