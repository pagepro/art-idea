import { TextProps } from 'theme-ui';

export interface HeadingProps extends TextProps {
  title?: string;
}
