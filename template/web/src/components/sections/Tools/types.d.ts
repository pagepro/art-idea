import { PresetProps } from 'components/molecules/Preset/types';
import { HeroProps } from 'components/organisms/Hero/types';

export interface ToolsProps extends PresetProps {
  label: string;
  href: string;
  onChangePreset: (banner: HeroProps) => void;
}
