export interface CMSProps {
  title: string;
  action?: JSX.Element;
  content: string;
}
