export type IPageData = {
  data: {
    page: IPage;
  };
};
export type IPage = {
  title: string;
  _rawContent: IContent[];
  buttonLink: string;
  buttonText: string;
  seoTitle: string;
  seoDescription: string;
  h1Text: string;
  descriptionText: string;
  adCode: string;
  showBriefGenerator: boolean;
  subtitleText: string;
  subtitleUrl: string;
  presetsButtonText: string;
  presetsButtonUrl: string;
};

export type IToolButton = {
  href: string;
  label: string;
};
