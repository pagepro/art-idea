import { GatsbyFluidImageProps } from 'gatsby-source-sanity';
import { IToolButton } from '../page/types';

export type DataQuery = {
  categories: {
    nodes: ICategory[];
  };
  banners: {
    nodes: IBanner[];
  };
  presets: {
    nodes: IPreset[];
  };
};
export interface ICategory {
  id: string;
  name: string;
  parametersList: string[];
}

export interface ICategoryItem extends Omit<ICategory, 'parametersList'> {
  value: string;
  label: string;
  selected: boolean;
  otherPossibilitiesCount?: number | null;
  items: string[];
  disabled: boolean;
  category?: string;
}

export type IBriefSectionHeading = {
  heading: string;
  button: string;
};

export type IBanner = {
  id: string;
  title: string;
  text: string;
  url: string;
  image: IImageHotspot;
};
interface IImage {
  _type: 'image';
  _key: string;
  asset: {
    fluid: GatsbyFluidImageProps;
  };
}
interface IImageHotspot extends IImage {
  hotspot: IHotspot;
}

export type IPreset = {
  id: string;
  name: string;
  categories: ICategory[];
  headerPicture: IBanner;
};

export interface IHomePageBody {
  adCode?: string;
  presetsButton: IToolButton;
}
