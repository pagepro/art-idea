export interface SEOProps {
  description?: string;
  lang?: string;
  title?: string;
}
