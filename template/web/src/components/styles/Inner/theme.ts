export default {
  container: {
    paddingX: ['1.875rem', '2rem', '2.5rem'],
    variant: 'layout.container',
  },
};
