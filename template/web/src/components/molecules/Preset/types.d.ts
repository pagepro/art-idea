import { HeroProps } from 'components/organisms/Hero/types';

export interface PresetProps {
  onChangePreset: (banner: HeroProps) => void;
}
