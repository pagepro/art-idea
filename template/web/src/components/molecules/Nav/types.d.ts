import { NavLinkProps } from 'theme-ui';

export interface NavItem {
  label: string;
  href: string;
  key: string;
  isExternalLink?: boolean;
}

export interface NavProps extends NavLinkProps {
  items: NavItem[];
}
