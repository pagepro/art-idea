export interface ActionProps {
  actionLabel: string;
  label: string;
  adCode?: string;
  onGenerateBriefClick: () => void;
}
