export interface ParametersSelectProps {
  id: string;
  disabled?: boolean;
  onRandomizeClick?: () => void;
}
