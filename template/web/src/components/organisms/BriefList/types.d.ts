export interface BriefListItem {
  id: string;
  category?: string;
  items: string[];
  otherPossibilitiesCount?: number | null;
  disabled?: boolean;
}

export type OnItemSelect = (
  event: 'click' | 'mouseLeave',
  id: string,
  columnItems: string[],
  itemIndex: number,
) => void;

export interface BriefListProps {
  animationIds?: string[];
  setAnimationIds?: React.Dispatch<React.SetStateAction<string[]>>;
}
