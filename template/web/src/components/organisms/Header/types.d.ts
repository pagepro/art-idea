export interface HeaderProps {
  siteTitle: string;
  subtitleText?: string;
  subtitleUrl?: string;
}
