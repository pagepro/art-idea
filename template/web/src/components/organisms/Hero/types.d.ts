import { GatsbyFluidImageProps } from 'gatsby-source-sanity';

export interface HeroProps {
  id: string;
  image: GatsbyFluidImageProps;
  hotspot: IHotspot;
  title: string;
  subtitle?: string;
  subtitleUrl?: string;
  onClickRefresh?: VoidFunction;
}
