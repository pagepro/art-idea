import IconLocked from './locked.svg';
import IconLockedEmpty from './locked-empty.svg';
import IconHidden from './hidden.svg';
import IconVisible from './visible.svg';
import IconArrows from './arrows.svg';
import IconRandomize from './randomize.svg';
import IconRandomizeCircle from './randomize-circle.svg';
import IconPlus from './plus.svg';
import IconMinus from './minus.svg';
import IconQuestion from './question.svg';

export {
  IconLocked,
  IconLockedEmpty,
  IconHidden,
  IconVisible,
  IconArrows,
  IconRandomize,
  IconRandomizeCircle,
  IconPlus,
  IconMinus,
  IconQuestion,
};
