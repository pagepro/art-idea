import theme from '../gatsby-plugin-theme-ui/index';

const [phone, tablet, desktop] = theme.breakpoints;

const { smallPhone } = theme.sizes;

const media = {
  smallPhone: `(max-width: ${smallPhone})`,
  phone: `(min-width: ${phone})`,
  tablet: `(min-width: ${tablet})`,
  desktop: `(min-width: ${desktop})`,
};

export default media;
