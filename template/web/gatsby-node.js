exports.onCreateWebpackConfig = ({ actions, getConfig }) => {
  const config = getConfig();

  config.resolve.extensions.push('.d.ts');

  actions.replaceWebpackConfig(config);
};

const createPages = async (graphql, actions) => {
  const { createPage } = actions;
  const result = await graphql(`
    {
      allSanityPage {
        nodes {
          id
          slug {
            current
          }
        }
      }
    }
  `);

  if (result.errors) throw result.errors;

  const pages = (result.data.allSanityPage || {}).nodes || [];
  pages.forEach((page) => {
    const { id, slug } = page;
    const path = `${slug.current}`;

    createPage({
      path,
      component: require.resolve('./src/templates/page.tsx'),
      context: { id },
    });
  });
};

exports.createPages = async ({ graphql, actions }) => {
  await createPages(graphql, actions);
};
