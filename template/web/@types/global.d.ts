import { FluidObject } from 'gatsby-image';
import { SxStyleProp } from 'theme-ui';

declare global {
  type SvgComponentType = React.FunctionComponent<
    React.SVGProps<SVGSVGElement> & { sx?: SxStyleProp }
  >;

  interface ChildImageSharpFluid {
    childImageSharp: {
      fluid: FluidObject;
    };
  }

  type GQLImages<N> = Record<N, ChildImageSharpFluid>;

  type IContent = {
    _key: string;
    _type: string;
    children: [
      {
        _key: string;
        _type: string;
        marks: ['mark'];
        text: string;
      },
    ];
    markDefs: [];
    style: string;
  };

  type IHotspot = {
    x: number;
    y: number;
  };
}
